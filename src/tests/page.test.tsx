import React from 'react'
import { Store } from 'redux'
import Error from 'next/error'
import { shallow } from 'enzyme'
import { Entry } from 'contentful'
import { SingletonRouter } from 'next/router'
import { PageComponent } from '../pages/page'
import { getComponentByType } from '../shared'
import { mockEntryFactory, mockStoreFactory } from '../__mocks__'
import { IPageFields } from '../types'
import { AppState } from '../store'
import * as ContentTypes from '../contentTypes'
import * as Entries from '../entries'

jest.mock('../contentTypes')
jest.mock('../shared')
jest.mock('../entries')
jest.mock('../contentful.setup')

const MockEntries = Entries as jest.Mocked<typeof Entries>
const MockContentTypes = ContentTypes as jest.Mocked<typeof ContentTypes>

describe('>>> PageComponent Component', () => {
  describe('>> render', () => {
    let pages: Array<Entry<IPageFields<unknown>>>
    let router: SingletonRouter

    beforeEach(() => {
      const fileds1: IPageFields<unknown> = {
        layout: [
          mockEntryFactory({})
        ],
        title: 'title1'
      }

      const fileds2: IPageFields<unknown> = {
        layout: [
          mockEntryFactory({}),
          mockEntryFactory({})
        ],
        title: 'title2'
      }

      const fileds3: IPageFields<unknown> = {
        layout: [
          mockEntryFactory({}),
          mockEntryFactory({}),
          mockEntryFactory({})
        ],
        title: 'title3'
      }

      pages = [
        mockEntryFactory(fileds1),
        mockEntryFactory(fileds2),
        mockEntryFactory(fileds3)
      ]

      router = {
        router: {} as any,
        readyCallbacks: [jest.fn()],
        ready: jest.fn(),
        pathname: '',
        asPath: '',
        route: '',
        query: {} as any,
        push: jest.fn(),
        replace: jest.fn(),
        reload: jest.fn(),
        back: jest.fn(),
        prefetch: jest.fn(),
        beforePopState: jest.fn(),
        events: {} as any
      }
    })

    it('should wait until data is loaded', () => {
      const wrapper1 = shallow(<PageComponent pages={[]} router={router} entries={[]} contentTypes={[]} />)
      expect(wrapper1.getElement()).toBe(null)

      const wrapper2 = shallow(<PageComponent pages={null as any} router={router} entries={[]} contentTypes={[]} />)
      expect(wrapper2.getElement()).toBe(null)
    })

    it('should render 404 if page doesn\'t exist', () => {
      router.asPath = 'unknown'
      const wrapper = shallow(<PageComponent pages={pages} router={router} entries={[]} contentTypes={[]} />)
      expect(wrapper.find(Error).length).toBe(1)
    })

    it('should render title', () => {
      router.asPath = 'title2'
      const wrapper = shallow(<PageComponent pages={pages} router={router} entries={[]} contentTypes={[]} />)
      expect(wrapper.find('h3').text()).toBe(router.asPath)
    })

    it('should render layout for each page if data is correct', () => {
      const page = pages[1]
      router.asPath = page.fields.title
      const spy = jest.spyOn(PageComponent, 'renderLayout')
      shallow(<PageComponent pages={pages} router={router} entries={[]} contentTypes={[]} />)
      expect(spy).toBeCalledTimes(page.fields.layout.length)

    })
  })

  describe('>> renderLayout', () => {
    it('should call getComponentByType if all data is provided', () => {
      const entry = mockEntryFactory({})

      PageComponent.renderLayout(entry)
      expect(getComponentByType).toBeCalled()
    })

    it('should call getComponentByType if entry is invalid', () => {
      const entry = {} as Entry<unknown>

      PageComponent.renderLayout(entry)
      expect(getComponentByType).not.toBeCalled()

      const entry1 = mockEntryFactory({})
      entry1.sys.contentType.sys = null as any
      PageComponent.renderLayout(entry1)
      expect(getComponentByType).not.toBeCalled()
    })
  })

  describe('>> getInitialProps', () => {
    const store: Store = mockStoreFactory()
    const state: AppState = {
      contentTypes: {
        items: {},
        isFetching: false
      },
      entries: {
        items: {},
        isFetching: false
      }
    }
    store.getState = jest.fn().mockReturnValue(state)

    it('should dispatch contentTypesFetchAction and entriesFetchAction', async () => {
      MockEntries.getAllEntries.mockReturnValueOnce([])
      MockContentTypes.getAllContentTypes.mockReturnValue([])

      await PageComponent.getInitialProps({ store, pathname: '', query: {} })
      expect(store.dispatch).toBeCalledTimes(2)
      expect(MockContentTypes.contentTypesFetchAction).toBeCalled()
      expect(MockEntries.entriesFetchAction).toBeCalled()
    })

    it('should not dispatch if data is already in the store', async () => {
      MockEntries.getAllEntries.mockReturnValueOnce([{} as any])
      MockContentTypes.getAllContentTypes.mockReturnValue([{} as any])

      await PageComponent.getInitialProps({ store, pathname: '', query: {} })
      expect(store.dispatch).toBeCalledTimes(0)
    })
  })
})
