import { all, fork, takeLatest, put, call } from 'redux-saga/effects'
import { EntryCollection } from 'contentful'
import { entriesFetchSuccessAction, entriesFetchFailureAction } from './actions'
import { ENTRIES_FETCH, IEntryFetch } from './types'
import { fetchEntries } from './api'

export function* entriesFetchFlow(action: IEntryFetch) {
  try {
    const response: EntryCollection<unknown> = yield call(fetchEntries, action.payload)
    yield put(entriesFetchSuccessAction(action.payload, response))
  } catch (e) {
    yield put(entriesFetchFailureAction(action.payload, e))
  }
}

export function* watchEntriesFetch() {
  yield takeLatest(ENTRIES_FETCH, entriesFetchFlow)
}

export default function* () {
  yield all([
    fork(watchEntriesFetch)
  ])
}
