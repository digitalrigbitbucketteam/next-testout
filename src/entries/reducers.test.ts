import { EntryCollection } from 'contentful'
import Reducer, { initialState } from './reducers'
import {
  IEntryFetchPayload,
  IEntryFetchError,
  IEntryState,
  EntryActionType
} from './types'

import {
  entriesFetchAction,
  entriesFetchSuccessAction,
  entriesFetchFailureAction
} from './actions'
import { mockEntryFactory } from '../__mocks__'

describe('>>> Entries Reducer', () => {
  describe('>> entriesFetch', () => {
    const payload: IEntryFetchPayload = { include: 1 }
    let initialStateMock: IEntryState

    beforeEach(() => {
      initialStateMock = {
        items: {},
        isFetching: false,
        fetchError: undefined
      }
    })

    it('should use initial state if none is provided', () => {
      expect(Reducer(undefined, {} as any)).toEqual(initialState)
    })

    it('should handle default', () => {
      const expectedState = initialStateMock
      const action = {}
      expect(Reducer(initialStateMock, action as any)).toEqual(expectedState)
    })

    it('should handle ENTRIES_FETCH', () => {
      const action: EntryActionType = entriesFetchAction(payload)
      const expectedState: IEntryState = {
        ...initialStateMock,
        isFetching: true,
        fetchError: undefined
      }

      expect(Reducer(initialStateMock, action)).toEqual(expectedState)
    })

    it('should handle ENTRIES_FETCH_SUCCESS', () => {
      const response: EntryCollection<unknown> = {
        items: [mockEntryFactory({})],
        total: 1,
        skip: 1,
        limit: 1,
        stringifySafe: jest.fn(),
        toPlainObject: jest.fn()
      }

      const action: EntryActionType = entriesFetchSuccessAction(payload, response)
      const expectedState: IEntryState = {
        ...initialStateMock,
        items: action.response.items.reduce((item, elm) => ({ ...item, [elm.sys.id]: elm }), {})
      }

      expect(Reducer(initialStateMock, action)).toEqual(expectedState)
    })

    it('should handle ENTRIES_FETCH_FAILURE', () => {
      const error: IEntryFetchError = { message: 'error' }
      const action: EntryActionType = entriesFetchFailureAction(payload, error)
      const expectedState = {
        ...initialStateMock,
        isFetching: false,
        fetchError: error
      }

      expect(Reducer(initialStateMock, action)).toEqual(expectedState)
    })
  })
})
