import { AppState } from '../store'
import { Entry } from 'contentful'
import { IPageFields } from '../types'

export const getAllEntries = (state: AppState): Array<Entry<unknown>> => Object.values(state.entries.items)

export const getPages = (state: AppState): Array<Entry<IPageFields<unknown>>> =>
  Object
    .values(state.entries.items)
    .filter((item) => item.sys.contentType.sys.id === 'page') as Array<Entry<IPageFields<unknown>>>

export const getPageByTitle = (state: AppState, title: string): Entry<IPageFields<unknown>> | undefined => {
  return Object
    .values(state.entries.items)
    .find((item) => {
      const fields = item.fields as IPageFields<unknown>
      return item.sys.contentType.sys.id === 'page' && fields.title === title
    }) as Entry<IPageFields<unknown>>
}
