import { getAllEntries, getPages, getPageByTitle } from './selectors'
import { IEntryState } from './types'
import { mockEntryFactory, mockContentTypeLinkFactory, mockSysFactory } from '../__mocks__'

describe('>>> Entries selectors', () => {
  describe('>> getAllEntries', () => {
    it('should return all entries from the state', () => {
      const entries = { items: {}, isFetching: false }
      const state = { entries, other: {} }

      expect(getAllEntries(state as any)).toEqual(Object.values(entries.items))
    })
  })

  describe('>> search for pages', () => {
    let state = {}
    let entries: IEntryState
    beforeEach(() => {
      const pageContentTypeLink = mockContentTypeLinkFactory('page')
      const otherContentTypeLink = mockContentTypeLinkFactory('?')

      const entry1 = mockEntryFactory({ title: 'title1' }, mockSysFactory('1', otherContentTypeLink))
      const entry2 = mockEntryFactory({ title: 'title2' }, mockSysFactory('2', otherContentTypeLink))
      const entry3 = mockEntryFactory({ title: 'title3' }, mockSysFactory('3', pageContentTypeLink))
      const entry4 = mockEntryFactory({ title: 'title4' }, mockSysFactory('4', otherContentTypeLink))
      const entry5 = mockEntryFactory({ title: 'title5' }, mockSysFactory('5', pageContentTypeLink))
      const entry6 = mockEntryFactory({ title: 'title6' }, mockSysFactory('6', otherContentTypeLink))

      entries = {
        items: {
          1: entry1,
          2: entry2,
          3: entry3,
          4: entry4,
          5: entry5,
          6: entry6
        },
        isFetching: false
      }

      state = { entries, other: {} }
    })
    describe('>> getPages', () => {
      it('should return all pages from the state', () => {
        expect(getPages(state as any)).toEqual([entries.items[3], entries.items[5]])
      })
    })

    describe('>> getPageByTitle', () => {
      it('should return pages by provided title from the state', () => {
        expect(getPageByTitle(state as any, 'title3')).toEqual(entries.items[3])
      })
    })
  })

})
