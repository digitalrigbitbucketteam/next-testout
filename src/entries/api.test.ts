import { fetchEntries } from './api'
import { IEntryFetchPayload } from './types'
import { client } from '../contentful.setup'

jest.mock('../contentful.setup')

describe('>>> Entries API', () => {
  describe('>> fetchEntries', () => {
    it('should fetch', () => {
      const payload: IEntryFetchPayload = { include: 1 }
      fetchEntries(payload)

      expect(client.getEntries).toBeCalled()
    })
  })
})
