import {
  ENTRIES_FETCH,
  ENTRIES_FETCH_SUCCESS,
  ENTRIES_FETCH_FAILURE,
  EntryActionType,
  IEntryState
} from './types'

export const initialState: IEntryState = {
  items: {},
  isFetching: false,
  fetchError: undefined
}

export default (state = initialState, action: EntryActionType): IEntryState => {
  switch (action.type) {
    case ENTRIES_FETCH:
      return {
        ...state,
        isFetching: true
      }
    case ENTRIES_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.response.items.reduce((item, elm) => ({ ...item, [elm.sys.id]: elm }), {})
      }
    case ENTRIES_FETCH_FAILURE:
      return {
        ...state,
        isFetching: false,
        fetchError: action.error
      }
    default:
      return state
  }
}
