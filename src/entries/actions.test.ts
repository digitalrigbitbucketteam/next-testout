import { EntryCollection } from 'contentful'
import {
  ENTRIES_FETCH,
  ENTRIES_FETCH_SUCCESS,
  ENTRIES_FETCH_FAILURE,
  IEntryFetchPayload,
  IEntryFetchError,
} from './types'

import {
  entriesFetchAction,
  entriesFetchSuccessAction,
  entriesFetchFailureAction
} from './actions'

describe('>>> cadidatesFetchAction', () => {
  const payload: IEntryFetchPayload = {
    include: 2
  }

  it('should contain the right action type', () => {
    const action = entriesFetchAction(payload)

    expect(action.type).toEqual(ENTRIES_FETCH)
    expect(action.payload).toEqual(payload)
  })
})

describe('>>> entriesFetchSuccessAction', () => {
  const payload: IEntryFetchPayload = {
    include: 1
  }

  const response: EntryCollection<unknown> = {
    items: [],
    total: 1,
    skip: 1,
    limit: 1,
    stringifySafe: jest.fn(),
    toPlainObject: jest.fn()
  }

  it('should contain the right action type', () => {
    const action = entriesFetchSuccessAction(payload, response)

    expect(action.type).toEqual(ENTRIES_FETCH_SUCCESS)
    expect(action.payload).toEqual(payload)
  })
})

describe('>>> entriesFetchFailureAction', () => {
  const payload: IEntryFetchPayload = {
    include: 1
  }

  const error: IEntryFetchError = {
    message: 'error'
  }

  it('should contain the right action type', () => {
    const action = entriesFetchFailureAction(payload, error)

    expect(action.type).toEqual(ENTRIES_FETCH_FAILURE)
    expect(action.payload).toEqual(payload)
  })
})
