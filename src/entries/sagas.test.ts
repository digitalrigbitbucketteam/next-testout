import { takeLatest, call, put, all, fork } from 'redux-saga/effects'
import { EntryCollection } from 'contentful'
import rootSaga, { entriesFetchFlow, watchEntriesFetch } from './sagas'
import { entriesFetchSuccessAction, entriesFetchFailureAction } from './actions'
import {
  ENTRIES_FETCH,
  IEntryFetch,
  IEntryFetchPayload,
  IEntryFetchError
} from './types'
import { fetchEntries } from './api'

jest.mock('../contentful.setup')

describe('>>> Entry Saga', () => {
  describe('>> root saga', () => {
    it('should combine all sagas together', () => {
      expect(rootSaga().next().value).toEqual(all([
        fork(watchEntriesFetch)
      ]))
    })
  })

  describe('>> entriesFetchFlow', () => {
    it('should call entriesFetchFlow', () => {
      const payload = { include: 1 }
      const action: IEntryFetch = { type: ENTRIES_FETCH, payload }
      const gen = entriesFetchFlow(action)

      expect(gen.next().value).toEqual(call(fetchEntries, payload))
    })

    it('should dispatch entriesFetchSuccessAction', () => {
      const response: EntryCollection<unknown> = {
        items: [],
        total: 1,
        skip: 1,
        limit: 1,
        stringifySafe: jest.fn(),
        toPlainObject: jest.fn()
      }
      const payload: IEntryFetchPayload = {
        include: 1
      }
      const action: IEntryFetch = { type: ENTRIES_FETCH, payload }
      const gen = entriesFetchFlow(action)
      gen.next()
      expect(gen.next(response).value).toEqual(put(entriesFetchSuccessAction(payload, response)))
    })

    it('should dispatch spaceFailureAction', () => {
      const error: IEntryFetchError = { message: 'message' }
      const payload = { include: 1 }
      const action: IEntryFetch = { type: ENTRIES_FETCH, payload }
      const gen = entriesFetchFlow(action)
      gen.next()

      expect(gen.throw!(error).value).toEqual(put(entriesFetchFailureAction(payload, error)))
    })
  })

  describe('watchEntriesFetch', () => {
    it('should call entriesFetchFlow', () => {
      const gen = watchEntriesFetch()
      expect(gen.next().value).toEqual(takeLatest(ENTRIES_FETCH, entriesFetchFlow))
    })
  })
})
