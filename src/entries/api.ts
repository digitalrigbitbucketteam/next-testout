import { IEntryFetchPayload } from './types'
import { client } from '../contentful.setup'
import { EntryCollection } from 'contentful'

export const fetchEntries = (payload: IEntryFetchPayload): Promise<EntryCollection<unknown>> => {
  return client.getEntries(payload)
}
