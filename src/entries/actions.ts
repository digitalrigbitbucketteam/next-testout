import { EntryCollection } from 'contentful'
import {
  ENTRIES_FETCH,
  ENTRIES_FETCH_SUCCESS,
  ENTRIES_FETCH_FAILURE,
  IEntryFetchPayload,
  IEntryFetchError,
  EntryActionType,
  IEntryFetchSuccess,
  IEntryFetchFailure
} from './types'

export const entriesFetchAction = (
  payload: IEntryFetchPayload
): EntryActionType => {
  return {
    type: ENTRIES_FETCH,
    payload
  }
}

export const entriesFetchSuccessAction = (
  payload: IEntryFetchPayload,
  response: EntryCollection<unknown>
): IEntryFetchSuccess => {
  return {
    type: ENTRIES_FETCH_SUCCESS,
    payload,
    response
  }
}

export const entriesFetchFailureAction = (
  payload: IEntryFetchPayload,
  error: IEntryFetchError
): IEntryFetchFailure => {
  return {
    type: ENTRIES_FETCH_FAILURE,
    payload,
    error
  }
}
