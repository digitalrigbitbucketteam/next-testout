import { EntryCollection, Entry } from 'contentful'

export const ENTRIES_FETCH = 'ENTRIES/FETCH'
export const ENTRIES_FETCH_SUCCESS = 'ENTRIES/FETCH_SUCCESS'
export const ENTRIES_FETCH_FAILURE = 'ENTRIES/FETCH_FAILURE'

export interface IEntryFetch {
  type: typeof ENTRIES_FETCH,
  payload: IEntryFetchPayload
}

export interface IEntryFetchSuccess {
  type: typeof ENTRIES_FETCH_SUCCESS,
  payload: IEntryFetchPayload,
  response: EntryCollection<unknown>
}

export interface IEntryFetchFailure {
  type: typeof ENTRIES_FETCH_FAILURE,
  payload: IEntryFetchPayload,
  error: IEntryFetchError
}

export interface IEntryFetchPayload {
  include: number
}

export interface IEntryFetchError {
  message: string
}

export interface IEntryState {
  items: {
    [id: string]: Entry<unknown>
  },
  isFetching: boolean
  fetchError?: IEntryFetchError
}

export type EntryActionType = IEntryFetch | IEntryFetchSuccess | IEntryFetchFailure
