import { all, fork, takeLatest, put, call } from 'redux-saga/effects'
import { ContentfulCollection, ContentType } from 'contentful'
import { contentTypesFetchSuccessAction, contentTypesFetchFailureAction } from './actions'
import { CONTENT_TYPES_FETCH, IContentTypeFetch } from './types'
import { fetchContentTypes } from './api'

export function* contentTypesFetchFlow(action: IContentTypeFetch) {
  try {
    const response: ContentfulCollection<ContentType> = yield call(fetchContentTypes, action.payload)
    yield put(contentTypesFetchSuccessAction(action.payload, response))
  } catch (e) {
    yield put(contentTypesFetchFailureAction(action.payload, e))
  }
}

export function* watchContentTypesFetch() {
  yield takeLatest(CONTENT_TYPES_FETCH, contentTypesFetchFlow)
}

export default function* () {
  yield all([
    fork(watchContentTypesFetch)
  ])
}
