import { ContentType } from 'contentful'
import { AppState } from '../store'

export const getAllContentTypes = (state: AppState): ContentType[] => Object.values(state.contentTypes.items)
