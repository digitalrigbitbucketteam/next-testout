import { takeLatest, put, call, all, fork } from 'redux-saga/effects'
import { ContentfulCollection, ContentType } from 'contentful'
import rootSaga, {
  contentTypesFetchFlow,
  watchContentTypesFetch
} from './sagas'
import {
  contentTypesFetchSuccessAction,
  contentTypesFetchFailureAction
} from './actions'
import {
  CONTENT_TYPES_FETCH,
  IContentTypeFetch,
  IContentTypeFetchPayload,
  IContentTypeFetchError
} from './types'
import { fetchContentTypes } from './api'

jest.mock('../contentful.setup')

describe('>>> ContentType Saga', () => {
  describe('>> root saga', () => {
    it('should combine all sagas together', () => {
      expect(rootSaga().next().value).toEqual(all([
        fork(watchContentTypesFetch)
      ]))
    })
  })

  describe('>> contentTypesFetchFlow', () => {
    const payload: IContentTypeFetchPayload = {}

    it('should call contentTypesFetchFlow', () => {
      const action: IContentTypeFetch = { type: CONTENT_TYPES_FETCH, payload }
      const gen = contentTypesFetchFlow(action)

      expect(gen.next().value).toEqual(call(fetchContentTypes, payload))
    })

    it('should dispatch contentTypesFetchSuccessAction', () => {
      const response: ContentfulCollection<ContentType> = {
        items: [],
        total: 1,
        skip: 1,
        limit: 1,
        toPlainObject: jest.fn()
      }

      const action: IContentTypeFetch = { type: CONTENT_TYPES_FETCH, payload }
      const gen = contentTypesFetchFlow(action)
      gen.next()
      expect(gen.next(response).value).toEqual(put(contentTypesFetchSuccessAction(payload, response)))
    })

    it('should dispatch contentTypeFailureAction', () => {
      const error: IContentTypeFetchError = { message: 'message' }
      const action: IContentTypeFetch = { type: CONTENT_TYPES_FETCH, payload }
      const gen = contentTypesFetchFlow(action)
      gen.next()

      expect(gen.throw!(error).value).toEqual(put(contentTypesFetchFailureAction(payload, error)))
    })
  })

  describe('watchContentTypesFetch', () => {
    it('should call contentTypesFetchFlow', () => {
      const gen = watchContentTypesFetch()
      expect(gen.next().value).toEqual(takeLatest(CONTENT_TYPES_FETCH, contentTypesFetchFlow))
    })
  })
})
