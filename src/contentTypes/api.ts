import { IContentTypeFetchPayload } from './types'
import { client } from '../contentful.setup'
import { ContentfulCollection, ContentType } from 'contentful'

export const fetchContentTypes = (payload: IContentTypeFetchPayload): Promise<ContentfulCollection<ContentType>> => {
  return client.getContentTypes(payload)
}
