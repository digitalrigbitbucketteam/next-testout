import {
  CONTENT_TYPES_FETCH,
  CONTENT_TYPES_FETCH_SUCCESS,
  CONTENT_TYPES_FETCH_FAILURE,
  IContentTypeFetchPayload,
  IContentTypeFetchError,
  ContentTypeActionType,
  IContentTypeFetchSuccess,
  IContentTypeFetchFailure
} from './types'
import { ContentfulCollection, ContentType } from 'contentful'

export const contentTypesFetchAction = (
  payload: IContentTypeFetchPayload
): ContentTypeActionType => {
  return {
    type: CONTENT_TYPES_FETCH,
    payload
  }
}

export const contentTypesFetchSuccessAction = (
  payload: IContentTypeFetchPayload,
  response: ContentfulCollection<ContentType>
): IContentTypeFetchSuccess => {
  return {
    type: CONTENT_TYPES_FETCH_SUCCESS,
    payload,
    response
  }
}

export const contentTypesFetchFailureAction = (
  payload: IContentTypeFetchPayload,
  error: IContentTypeFetchError
): IContentTypeFetchFailure => {
  return {
    type: CONTENT_TYPES_FETCH_FAILURE,
    payload,
    error
  }
}
