import { getAllContentTypes } from './selectors'

describe('>>> ContentTypes selectors', () => {
  describe('>> getAllContentTypes', () => {
    it('should return all contentTypes from the state', () => {
      const contentTypes = { items: {}, isFetching: false }
      const state = { contentTypes, other: {} }

      expect(getAllContentTypes(state as any)).toEqual(Object.values(contentTypes.items))
    })
  })
})
