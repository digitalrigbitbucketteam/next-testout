import {
  CONTENT_TYPES_FETCH,
  CONTENT_TYPES_FETCH_SUCCESS,
  CONTENT_TYPES_FETCH_FAILURE,
  ContentTypeActionType,
  IContentTypeState
} from './types'

export const initialState: IContentTypeState = {
  items: {},
  isFetching: false,
  fetchError: undefined
}

export default (state = initialState, action: ContentTypeActionType): IContentTypeState => {
  switch (action.type) {
    case CONTENT_TYPES_FETCH:
      return {
        ...state,
        isFetching: true
      }
    case CONTENT_TYPES_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.response.items.reduce((item, elm) => ({ ...item, [elm.sys.id]: elm }), {})
      }
    case CONTENT_TYPES_FETCH_FAILURE:
      return {
        ...state,
        isFetching: false,
        fetchError: action.error
      }
    default:
      return state
  }
}
