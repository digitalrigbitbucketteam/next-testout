import { ContentfulCollection, ContentType } from 'contentful'
import Reducer, { initialState } from './reducers'
import {
  IContentTypeFetchPayload,
  IContentTypeFetchError,
  IContentTypeState,
  ContentTypeActionType
} from './types'

import {
  contentTypesFetchAction,
  contentTypesFetchSuccessAction,
  contentTypesFetchFailureAction
} from './actions'

describe('>>> ContentTypes Reducer', () => {
  describe('>> contentTypesFetch', () => {
    const payload: IContentTypeFetchPayload = {}
    let initialStateMock: IContentTypeState

    beforeEach(() => {
      initialStateMock = {
        items: {},
        isFetching: false,
        fetchError: undefined
      }
    })

    it('should use initial state if none is provided', () => {
      expect(Reducer(undefined, {} as any)).toEqual(initialState)
    })

    it('should handle default', () => {
      const expectedState = initialStateMock
      const action = {}
      expect(Reducer(initialStateMock, action as any)).toEqual(expectedState)
    })

    it('should handle CONTENT_TYPES_FETCH', () => {
      const action: ContentTypeActionType = contentTypesFetchAction(payload)
      const expectedState: IContentTypeState = {
        ...initialStateMock,
        isFetching: true,
        fetchError: undefined
      }

      expect(Reducer(initialStateMock, action)).toEqual(expectedState)
    })

    it('should handle CONTENT_TYPES_FETCH_SUCCESS', () => {
      const response: ContentfulCollection<ContentType> = {
        items: [{
          name: 'name',
          description: 'description',
          displayField: 'field',
          fields: [],
          toPlainObject: jest.fn(),
          sys: {
            type: 'string',
            id: 'string',
            createdAt: 'string',
            updatedAt: 'string',
            locale: 'string',
            contentType: {
              sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'string'
              }
            }
          }
        }],
        total: 1,
        skip: 1,
        limit: 1,
        toPlainObject: jest.fn()
      }
      const action: ContentTypeActionType = contentTypesFetchSuccessAction(payload, response)
      const expectedState: IContentTypeState = {
        ...initialStateMock,
        isFetching: false,
        items: action.response.items.reduce((item, elm) => ({ ...item, [elm.sys.id]: elm }), {})
      }

      expect(Reducer(initialStateMock, action)).toEqual(expectedState)
    })

    it('should handle CONTENT_TYPES_FETCH_FAILURE', () => {
      const error: IContentTypeFetchError = { message: 'error' }
      const action: ContentTypeActionType = contentTypesFetchFailureAction(payload, error)
      const expectedState = {
        ...initialStateMock,
        isFetching: false,
        fetchError: error
      }

      expect(Reducer(initialStateMock, action)).toEqual(expectedState)
    })
  })
})
