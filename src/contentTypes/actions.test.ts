import { ContentfulCollection, ContentType } from 'contentful'
import {
  CONTENT_TYPES_FETCH,
  CONTENT_TYPES_FETCH_SUCCESS,
  CONTENT_TYPES_FETCH_FAILURE,
  IContentTypeFetchPayload,
  IContentTypeFetchError,
} from './types'

import {
  contentTypesFetchAction,
  contentTypesFetchSuccessAction,
  contentTypesFetchFailureAction
} from './actions'

describe('>>> cadidatesFetchAction', () => {
  const payload: IContentTypeFetchPayload = {}

  it('should contain the right action type', () => {
    const action = contentTypesFetchAction(payload)

    expect(action.type).toEqual(CONTENT_TYPES_FETCH)
    expect(action.payload).toEqual(payload)
  })
})

describe('>>> contentTypesFetchSuccessAction', () => {
  const payload: IContentTypeFetchPayload = {}
  const response: ContentfulCollection<ContentType> = {
    items: [],
    total: 1,
    skip: 1,
    limit: 1,
    toPlainObject: jest.fn()
  }

  it('should contain the right action type', () => {
    const action = contentTypesFetchSuccessAction(payload, response)

    expect(action.type).toEqual(CONTENT_TYPES_FETCH_SUCCESS)
    expect(action.payload).toEqual(payload)
  })
})

describe('>>> contentTypesFetchFailureAction', () => {
  const payload: IContentTypeFetchPayload = {}

  const error: IContentTypeFetchError = {
    message: 'error'
  }

  it('should contain the right action type', () => {
    const action = contentTypesFetchFailureAction(payload, error)

    expect(action.type).toEqual(CONTENT_TYPES_FETCH_FAILURE)
    expect(action.payload).toEqual(payload)
  })
})
