import { ContentType, ContentfulCollection } from 'contentful'

export const CONTENT_TYPES_FETCH = 'CONTENT_TYPES/FETCH'
export const CONTENT_TYPES_FETCH_SUCCESS = 'CONTENT_TYPES/FETCH_SUCCESS'
export const CONTENT_TYPES_FETCH_FAILURE = 'CONTENT_TYPES/FETCH_FAILURE'

export interface IContentTypeFetch {
  type: typeof CONTENT_TYPES_FETCH,
  payload: IContentTypeFetchPayload
}

export interface IContentTypeFetchSuccess {
  type: typeof CONTENT_TYPES_FETCH_SUCCESS,
  payload: IContentTypeFetchPayload,
  response: ContentfulCollection<ContentType>
}

export interface IContentTypeFetchFailure {
  type: typeof CONTENT_TYPES_FETCH_FAILURE,
  payload: IContentTypeFetchPayload,
  error: IContentTypeFetchError
}

export interface IContentTypeFetchPayload {
  query?: string
}

export interface IContentTypeFetchError {
  message: string
}

export interface IContentTypeState {
  items: {
    [id: string]: ContentType
  },
  isFetching: boolean
  fetchError?: IContentTypeFetchError
}

export type ContentTypeActionType = IContentTypeFetch | IContentTypeFetchSuccess | IContentTypeFetchFailure
