import { fetchContentTypes } from './api'
import { IContentTypeFetchPayload } from './types'
import { client } from '../contentful.setup'

jest.mock('../contentful.setup')

describe('>>> ContentTypes API', () => {
  describe('>> fetchContentTypes', () => {
    it('should fetch', () => {
      const payload: IContentTypeFetchPayload = {}
      fetchContentTypes(payload)

      expect(client.getContentTypes).toBeCalled()
    })
  })
})
