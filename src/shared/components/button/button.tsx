import React from 'react'
import styled from 'styled-components'
import Link from 'next/link'

const Button = styled.button`
background: ${(props) => props.color === 'primary' ? '#02C874' : props.color = 'secondary' ? '#29BAFF' : 'black'};
border-radius: 4px;
color: white;
width: 200px;
padding: 10px;
cursor: pointer;
font-size: 18px;
`

export interface IButtonProps {
  value: string,
  url: string,
  color: string
}

export const ButtonComponent = ({ value, url, color }: IButtonProps) => {
  return (
    <Link href={url} >
      <a><Button color={color}>{value}</Button></a>
    </Link>
  )
}

export default ButtonComponent
