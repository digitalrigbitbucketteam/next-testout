import React from 'react'
import styled from 'styled-components'

const ImgBlock = styled.img`
  width: 200px;
  height: 200px;
  border: .5px solid gray;
`
export interface IImgBlockProps {
  src: string
}
export const ImgBlockComponent = ({ src }: IImgBlockProps) => {
  return (
    <ImgBlock src={src} style={{ width: 200 }} />
  )
}

export default ImgBlockComponent
