import React from 'react'
import styled from 'styled-components'

export const Header = styled.h1`
  font-size: 24px;
  text-align: center;
  color: #282828;
  font-weight: 400;
  line-height: 36px;
`

export interface IHeaderProps {
  value: string
}

export const HeaderComponent = ({ value }: IHeaderProps) => {
  return (
    <Header>{value}</Header >
  )
}

export default HeaderComponent
