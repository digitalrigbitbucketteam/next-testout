import React from 'react'
import { shallow } from 'enzyme'
import Component, { Header } from './header'

describe('>>> Header Component', () => {
  it('should styled Header', () => {
    expect(shallow(<Component value="some" />).find(Header).length).toBe(1)
  })
})
