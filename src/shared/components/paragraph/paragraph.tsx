import React from 'react'
import styled from 'styled-components'

const Paragraph = styled.p`
  font-size: 18px;
  line-height: 28px;
  text-align: center;
  color: #282828;
`

export interface IParagraphProps {
  text: string
}
export const ParagraphComponent = ({ text }: IParagraphProps) => {
  return (
    <Paragraph>{text}</Paragraph >
  )
}

export default ParagraphComponent
