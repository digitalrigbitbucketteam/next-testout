import React from 'react'
import { shallow } from 'enzyme'
import Component from './errorOnSpot'

describe('>>> ErrorOnSpot Component', () => {
  const errorMsg = 'error'

  it('should render message', () => {
    const rendred = shallow(<Component message={errorMsg} />)
    expect(rendred.find('div').text()).toBe(`!--${errorMsg}--!`)
  })

  describe('>> on production', () => {
    const oldEnv = process.env.NODE_ENV
    beforeEach(() => {
      // dirty, dirty hack to change read-only property
      process.env['NODE_ENV' as any] = 'production'
    })

    afterEach(() => {
      process.env['NODE_ENV' as any] = oldEnv
    })

    it('should not render on production', () => {
      const rendred = shallow(<Component message={errorMsg} />)
      expect(rendred.find('div').length).toBe(0)
    })

    it('should render on production if was forced to', () => {
      const rendred = shallow(<Component message={errorMsg} showOnProduction={true} />)
      expect(rendred.find('div').text()).toBe(`!--${errorMsg}--!`)
    })
  })
})
