import React from 'react'

export interface IErrorOnSpotProps {
  message: string,
  showOnProduction?: boolean
}

/**
 * Component to display error in specific point of page, "on spot"
 * Error won't be shown on production env unless explicitly specified
 *
 */
export const ErrorOnSpotComponent = ({ message, showOnProduction = false }: IErrorOnSpotProps) => {
  if (!showOnProduction && process.env.NODE_ENV === 'production') {
    return null
  }

  return (
    <div style={{ color: 'red' }}>!--{message}--!</div>
  )
}

export default ErrorOnSpotComponent
