import { Fragment } from 'react'
import {
  ParagraphComponent,
  IParagraphProps,
  ButtonComponent,
  IButtonProps,
  ImgBlockComponent,
  IImgBlockProps,
  HeaderComponent,
  IHeaderProps
} from '.'

export const getComponentByType = (type: string, fields: any): JSX.Element => {
  switch (type) {
    case 'paragraph': return <ParagraphComponent {...fields as IParagraphProps} />
    case 'button': return <ButtonComponent {...fields as IButtonProps} />
    case 'blockImage': return <ImgBlockComponent {...fields as IImgBlockProps} />
    case 'header': return <HeaderComponent {...fields as IHeaderProps} />
    default:
      if (process.env.NODE_ENV === 'production') {
        return <Fragment />
      } else {
        return <div style={{ color: 'red' }}>Component not found</div>
      }
  }
}
