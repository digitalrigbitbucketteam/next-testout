import React, { Fragment } from 'react'
import { getComponentByType } from './helper'

describe('>>> Shared Helper', () => {
  it('should return empty fragment in prod mode if provided type is not supported', () => {
    // dirty, dirty hack to change read-only property
    const oldEnv = process.env.NODE_ENV
    process.env['NODE_ENV' as any] = 'production'
    expect(getComponentByType('??', {})).toEqual(<Fragment />)
    process.env['NODE_ENV' as any] = oldEnv
  })
})
