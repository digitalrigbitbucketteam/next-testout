import { ContentTypeLink, Sys } from 'contentful'
import { mockContentTypeLinkFactory } from './mockContentTypeLinkFactory'

export const mockSysFactory = (
  id: string = '1',
  contentType: ContentTypeLink = mockContentTypeLinkFactory(),
  type: string = 'type',
  createdAt: string = 'createdAt',
  updatedAt: string = 'updatedAt',
  locale: string = 'locale'
): Sys => {
  return {
    type,
    id,
    createdAt,
    updatedAt,
    locale,
    contentType: {
      sys: contentType
    }
  }
}
