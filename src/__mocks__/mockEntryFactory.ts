import { Entry, Sys } from 'contentful'
import { mockSysFactory } from './mockSysFactory'

export function mockEntryFactory<T>(fields: T, sys: Sys = mockSysFactory()): Entry<T> {
  return {
    sys,
    fields,
    toPlainObject: jest.fn(),
    update: jest.fn()
  }
}
