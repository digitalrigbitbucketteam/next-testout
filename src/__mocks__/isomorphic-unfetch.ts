export const fetch = jest.fn().mockResolvedValue({
  json: jest.fn().mockResolvedValue([])
})

export default fetch
