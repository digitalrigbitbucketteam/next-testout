import { Store } from 'redux'

export function mockStoreFactory(): Store {
  return {
    dispatch: jest.fn(),
    getState: jest.fn(),
    subscribe: jest.fn(),
    replaceReducer: jest.fn(),
    [Symbol.observable]: jest.fn()
  }
}
