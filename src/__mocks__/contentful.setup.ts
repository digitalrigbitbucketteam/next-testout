export const client = {
  getSpace: jest.fn(),
  getEntries: jest.fn(),
  getContentTypes: jest.fn()

}

export default client
