import { ContentTypeLink } from 'contentful'

export const mockContentTypeLinkFactory = (id: string = '1'): ContentTypeLink => {
  return {
    id,
    type: 'Link',
    linkType: 'ContentType'
  }
}
