import { Entry } from 'contentful'

export interface IPageFields<T> {
  layout: Array<Entry<T> | undefined>
  title: string
}
