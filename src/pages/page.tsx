import React, { Fragment } from 'react'
import Error from 'next/error'
import { SingletonRouter, withRouter } from 'next/router'
import { Entry, ContentType } from 'contentful'
import { IPageFields } from '../types'
import { getComponentByType, ErrorOnSpotComponent } from '../shared'
import { Store } from 'redux'
import { NextPageContext } from 'next'
import { contentTypesFetchAction, getAllContentTypes } from '../contentTypes'
import { entriesFetchAction, getAllEntries, getPages } from '../entries'
import { AppState } from '../store'
import { connect } from 'react-redux'

export interface INextPageContextConnected extends NextPageContext {
  store: Store
}

export interface IPageProps {
  entries: Array<Entry<unknown>>,
  contentTypes: ContentType[],
  pages: Array<Entry<IPageFields<unknown>>>,
  router: SingletonRouter
}

/**
 * Generic Page Component
 * Determines if the requested page exists in Contentful data and if so rendres components inside it
 * Otherwise redirects to 404
 */
export class PageComponent extends React.Component<IPageProps> {

  /**
   * Makes initial preparations for each page: load bootstrapped data and prepares the store
   * Since this is static method we cannot use props in here, hence we cannot use ussual redux workflow
   * like this.props.dispatch(something). We haad to dispatch manually
   */
  static async getInitialProps({ store }: INextPageContextConnected) {
    const state = await store.getState()
    const entries = getAllEntries(state)
    const contentTypes = getAllContentTypes(state)

    if (entries.length > 0 && contentTypes.length > 0) {
      return {}
    }

    await store.dispatch(contentTypesFetchAction({}))
    await store.dispatch(entriesFetchAction({ include: 2 }))
    return {
      state
    }
  }

  /**
   * Determines if it's possible to render layout y specified data and rendres if so
   * Otherwise renders error component
   */
  static renderLayout(entry: Entry<unknown> | undefined): JSX.Element {
    if (!entry || !entry.sys || !entry.sys.contentType) {
      return <ErrorOnSpotComponent message="Component not found" />
    }

    const contentType = entry.sys.contentType
    if (!contentType.sys || !contentType.sys.id) {
      return <ErrorOnSpotComponent message="Component not found" />
    }

    return getComponentByType(contentType.sys.id, entry.fields)
  }

  render() {
    const { pages, router } = this.props

    if (!pages || pages.length < 1) { // wait until all data is loaded
      return null
    }

    const page = pages.find((item) => item.fields.title === router.asPath)
    if (!page) {
      return <Error statusCode={404} title="Page not found" />
    }

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          maxWidth: '375px',
          margin: '0 auto',
          padding: '50px 20px',
          backgroundColor: 'white'
        }}
      >
        <h3 style={{ textTransform: 'capitalize' }}>{page.fields.title}</h3>
        {page.fields.layout.map((layout: Entry<unknown> | undefined, index: number) => (
          <Fragment key={index}>{PageComponent.renderLayout(layout)}</Fragment>
        ))}
      </div>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    entries: getAllEntries(state),
    contentTypes: getAllContentTypes(state),
    pages: getPages(state)
  }
}

const mapDispatchToProps = () => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(PageComponent))
