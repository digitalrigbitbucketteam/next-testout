import Document from 'next/document'
import { ServerStyleSheet } from 'styled-components'
import { DocumentContext } from 'next/document'

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const sheet = new ServerStyleSheet()
    const originalRenderPage = ctx.renderPage

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />)
        })

      const initialProps = await Document.getInitialProps(ctx)
      const styles: any = (
        <>
          {initialProps.styles}
          {sheet.getStyleElement()}
          <style jsx={true} global={true}>{`
            @import url('https://fonts.googleapis.com/css?family=Roboto');
            body {
              margin: 0;
              font-family: 'Roboto', sans-serif;
              background-color: #e5e5e5
            }`}
          </style>
        </>
      )

      return {
        ...initialProps,
        styles
      }
    } finally {
      sheet.seal()
    }
  }
}
