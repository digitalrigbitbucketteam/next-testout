import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { all } from 'redux-saga/effects'
import { ContentTypesSagas, ContentTypesReducers } from './contentTypes'
import { EntriesReducers, EntriesSagas } from './entries'

const rootReducer = combineReducers({
  contentTypes: ContentTypesReducers,
  entries: EntriesReducers
})

export const makeStore = (initialState: object) => {
  let composeEnhancers = compose
  if (
    process.env.NODE_ENV !== 'production' &&
    typeof window !== 'undefined' &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
    composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  }

  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(sagaMiddleware))) as any
  store.sagaTask = sagaMiddleware.run(function* () {
    yield all([
      ContentTypesSagas(),
      EntriesSagas()
    ])
  })

  return store
}

export default makeStore

export type AppState = ReturnType<typeof rootReducer>
