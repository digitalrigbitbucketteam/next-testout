# EasyKnock Frontend App TestOut v0.0.1
NEXT.js playground for EasyKnock

## Install
First install Node.js (version is specified in package.json). Usage of nvm is recommended. Then: 
```
npm i
```
Create .env file (take .env.example as example) in root directory

## Dev start
```
npm run dev
```

## Testing and Linting
### Testing Regular
```
npm t
```

### Watcher
```
npm run test:watch 
```

### Testing Linting
```
npm run lint 
```

### Linting AutoFix
```
npm run lint:fix
```

### End-To-End Testing
```
npm run start:dev
npm run e2e 
```

### CI/CD Testing
```
npm run test:ci 
```

## Build:
```
npm run build
```

### Run Production Build
```
npm start
```

## Deployments 
- Test: https://gs-next-js.herokuapp.com
- PROD: 

## Tech stack
- React.js
- Redux.js
- Redux-saga.js
- Next.js
- TypeScript / TSLint
- Styled Components
- Jest / Enzyme 
- Cypress
- Contentful SDK

## Dev tools
- https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en
- https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd 

### If you use VSCode
- https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin
- https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig