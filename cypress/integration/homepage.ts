describe('>>> Home page', () => {
  beforeEach(() => {
    cy.viewport('iphone-6')
  })

  it('should contain basic content', () => {
    cy.visit('/')
    cy.get('h3').contains('/')
  })
})
