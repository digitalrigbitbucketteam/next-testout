describe('>>> Learn More page', () => {
  beforeEach(() => {
    cy.viewport('iphone-6')
  })

  it('should contain basic content', () => {
    cy.visit('/learn-more')
    cy.get('h3').contains('/learn-more')
  })
})
